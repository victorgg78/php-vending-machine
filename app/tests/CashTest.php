<?php
use PHPUnit\Framework\TestCase;
require 'src/Cash.php';

final class CashTest extends TestCase
{
    public function testAddCoin_1(): void
    {
        $cash = new Cash();
        $cash->addCoin(1);
        $this->assertEquals([1, 0, 0, 0], $cash->status());
    }

    public function testAddCoin_25(): void
    {
        $cash = new Cash();
        $cash->addCoin(.25);
        $this->assertEquals([0, 1, 0, 0], $cash->status());
    }
    
    public function testAddCoin_10(): void
    {
        $cash = new Cash();
        $cash->addCoin(.10);
        $this->assertEquals([0, 0, 1, 0], $cash->status());
    }
    
    public function testAddCoin_05(): void
    {
        $cash = new Cash();
        $cash->addCoin(.05);
        $this->assertEquals([0, 0, 0, 1], $cash->status());
    }    

    public function testAddCoin_invalid(): void
    {
        $cash = new Cash();
        
        $this->expectException(InvalidArgumentException::class);
        $cash->addCoin(.01);
    }
    
    public function testAddCoin_refund(): void
    {
        $cash = new Cash();
        $cash->addCoin(.05);
        $this->assertEquals([0, 0, 0, 1], $cash->rollback());    
        $this->assertEquals([0, 0, 0, 0], $cash->status());    
    }

    public function testAddCoin_refund_multiple(): void
    {
        $cash = new Cash();
        $cash->addCoin(.05);
        $cash->addCoin(1);
        $this->assertEquals([1, 0, 0, 1], $cash->rollback());    
        $this->assertEquals([0, 0, 0, 0], $cash->status());    
    }

    public function testAddCoin_commit(): void
    {
        $cash = new Cash();
        $cash->addCoin(1);
        $this->assertEquals([1, 0, 0, 0], $cash->status());
        $cash->commit();
        $this->assertEquals([1, 0, 0, 0], $cash->status());
    }

    public function testChange_1(): void
    {
        $cash = new Cash();
        $cash->addCoin(1);
        $cash->addCoin(.25);
        $cash->addCoin(.10);
        $cash->addCoin(.05);
        $this->assertEquals([1, 0, 0, 0], $cash->change(1));
    }

    public function testChange_5(): void
    {
        $cash = new Cash();
        $cash->addCoin(.05);
        $this->assertEquals([0, 0, 0, 1], $cash->change(.05));
    }
    
    public function testChange_1_small(): void
    {
        $cash = new Cash();
        $cash->addCoin(.25);
        $cash->addCoin(.25);
        $cash->addCoin(.25);
        $cash->addCoin(.25);
        $this->assertEquals([0, 4, 0, 0], $cash->change(1));
    }

    public function testChange_1_smaller(): void
    {
        $cash = new Cash();
        $cash->addCoin(.25);
        $cash->addCoin(.25);
        $cash->addCoin(.25);
        $cash->addCoin(.10);
        $cash->addCoin(.10);
        $cash->addCoin(.05);
        $this->assertEquals([0, 3, 2, 1], $cash->change(1));
    }

    public function testChange_2_25(): void
    {
        $cash = new Cash();
        $cash->addCoin(1);
        $cash->addCoin(1);
        $cash->addCoin(.25);
        $cash->addCoin(.25);
        $cash->addCoin(.10);
        $cash->addCoin(.10);
        $cash->addCoin(.05);
        $cash->addCoin(.05);
        $this->assertEquals([2, 1, 0, 0], $cash->change(2.25));
    }    

    public function testChange_2_65(): void
    {
        $cash = new Cash();
        $cash->addCoin(1);
        $cash->addCoin(1);
        $cash->addCoin(.25);
        $cash->addCoin(.25);
        $cash->addCoin(.05);
        $cash->addCoin(.05);
        $cash->addCoin(.05);
        $this->assertEquals([2, 2, 0, 3], $cash->change(2.65));
    }  
    
    public function testChange_no_change_is_needed(): void
    {
        $cash = new Cash();
        $cash->addCoin(1);
        $cash->addCoin(1);
        $cash->addCoin(.25);
        $cash->addCoin(.25);
        $cash->addCoin(.05);
        $cash->addCoin(.05);
        $cash->addCoin(.05);
        $this->assertEquals([0, 0, 0, 0], $cash->change(0));
    } 
    
    public function testChange_change_from_consolidated(): void
    {
        $cash = new Cash();
        $cash->addCoin(.10);
        $cash->addCoin(.10);
        $cash->addCoin(.10);
        $cash->addCoin(.05);
        $cash->commit();
        $this->assertEquals([0, 0, 3, 1], $cash->change(0.35));
    } 
    
    public function testChange_real_change(): void
    {
        $cash = new Cash();
        $cash->addCoin(.10);
        $cash->addCoin(.10);
        $cash->addCoin(.10);
        $cash->addCoin(.05);
        $cash->commit();
        $cash->addCoin(1);
        $this->assertEquals([0, 0, 3, 1], $cash->change(0.35));
    } 
}

?>