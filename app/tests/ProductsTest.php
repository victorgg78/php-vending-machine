<?php
use PHPUnit\Framework\TestCase;
require 'src/Products.php';

final class ProductsTest extends TestCase
{
    public function testSetStock(): void
    {
        $products = new Products();
        $this->assertEquals(-1, $products->get("WATER"));
        $products->set("WATER", 1);
        $this->assertEquals(0.65, $products->get("WATER"));
    }

    public function testReduceStock(): void
    {
        $products = new Products();
        $this->assertEquals(-1, $products->get("WATER"));
        $products->set("WATER", 1);
        $this->assertEquals(0.65, $products->get("WATER"));
        $products->reduce_stock("WATER");
        $this->assertEquals(-1, $products->get("WATER"));
    }
}

?>