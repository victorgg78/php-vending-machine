<?php

final class Products
{
    private $products = array(
        "WATER" => array(
            "price" => 0.65,
            "stock" => 0
        ),
        "JUICE" => array(
            "price" => 1,
            "stock" => 0
        ),
        "SODA" => array(
            "price" => 1.5,
            "stock" => 0
        ),
    );

    public function __construct()
    {
    }

    public function set(string $product, int $stock) {
        $this->products[$product]['stock'] = $stock;
    }

    public function get(string $product): float 
    {
        $inStock = $this->products[$product]['stock'] > 0;
        if (!$inStock) {
            return -1;
        }

        return $this->products[$product]['price'];
    }

    public function reduce_stock(string $product): void
    {
        $this->products[$product]['stock'] --;
    }
}


?>