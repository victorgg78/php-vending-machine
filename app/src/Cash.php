<?php

final class Cash
{
    // coins of 1., .25, .10, .05, confirmed-unconfirmed
    private $confirmed = array(0, 0, 0, 0);
    private $transaction = array(0, 0, 0, 0);

    public function __construct()
    {
        // to make status persistent, load it here!
    }

    public function addCoin($valueOfCoin): void
    {
        switch ($valueOfCoin) {
            case 1.:  $idx = 0; break;
            case .25: $idx = 1; break;
            case .10: $idx = 2; break;
            case .05: $idx = 3; break;
            default: 
                throw new InvalidArgumentException('coin of $valueOfCoin is not a valid coin!'); 
                break;
        };

        $this->transaction[$idx]++;
    }

    public function status(): array
    {
        $current = array(0, 0, 0, 0);
        foreach (range(0, 3) as $idx) {
            $current[$idx] = $this->confirmed[$idx] + $this->transaction[$idx];
        }
        return $current;
    }

    public function rollback(): array
    {
        $refund = $this->transaction;
        $this->transaction = array(0, 0, 0, 0);
        return $refund;
    }

    public function commit(): void
    {
        foreach (range(0, 3) as $idx) {
            $this->confirmed[$idx] += $this->transaction[$idx];
        }
        $this->transaction = array(0, 0, 0, 0);
    }

    public function amount(): float
    {
        return 1. * $this->transaction[0] + 
            0.25 * $this->transaction[1] +
            0.10 * $this->transaction[2] +
            0.05 * $this->transaction[3];
    }

    public function change($amount): array
    {
        // coins of 1., .25, .10, .05
        $coins = array(0, 0, 0, 0);
        $current = $this->status();

        $myAmount = $amount;
        foreach(array(100, 25, 10, 5) as $idx=>$value) {
            $coinsOf = (int)(($myAmount * 100. + 1) / $value);
            $coins[$idx] = ($coinsOf < $current[$idx]) ? $coinsOf : $current[$idx];
            $myAmount -= $coins[$idx] * ($value / 100.);
        }

        if (abs($myAmount) > 0.01) {
            throw new Exception("no change available! missing " . $myAmount);
        }

        $this->commit();
        foreach (range(0, 3) as $idx) {
            $this->confirmed[$idx] -= $coins[$idx];
        }

        return $coins;
    }
}


?>