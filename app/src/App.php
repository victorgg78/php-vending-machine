<?php
require 'Cash.php';
require 'Products.php';

final class App
{
    private $cash;
    private $products;

    public function __construct()
    {
        $this->cash = new Cash();
        $this->products = new Products();
    }

    private function to_money(array $coins): array
    {
        $mychange = array();
        foreach($coins as $idx=>$value) {
            if ($value > 0) {
                switch ($idx) {
                    case 0: $mychange = array_merge($mychange, array_fill(0, $value, "1")); break;
                    case 1: $mychange = array_merge($mychange, array_fill(0, $value, "0.25")); break;
                    case 2: $mychange = array_merge($mychange, array_fill(0, $value, "0.10")); break;
                    case 3: $mychange = array_merge($mychange, array_fill(0, $value, "0.05")); break;
                }
            }
        }

        return $mychange;
    }

    public function run(): void 
    {
        while ( FALSE !=($line=readline("vending-machine> ")) ) {
            $cmds = $this->parse_commands($line);

            foreach($cmds as $cmd) {
                switch ($cmd) {
                    case "0.05":
                    case "0.10":
                    case "0.25":
                    case "1":
                        $this->cash->addCoin(floatval($cmd));
                        break;

                    case "RETURN-COIN":
                        $refund = $this->cash->rollback();
                        print("-> " . join(", ", $this->to_money($refund)) . "\n");
                        break;

                    case "GET-WATER":
                    case "GET-JUICE":
                    case "GET-SODA":
                        $product = explode('-', $cmd)[1];
                        $price = $this->products->get($product);
                        if ($price == -1) {
                            print_r("-> not stock for product " . $product . "\n");
                            break;
                        }

                        $amount = $this->cash->amount();

                        if ($amount < $price) {
                            print_r("-> not enough money\n");
                            break;
                        }

                        $valueChange = $amount - $price;

                        try {
                            $change = $this->to_money($this->cash->change($valueChange));
                            print_r("-> " . join(", ", array_merge( (array)$product, $change)) . "\n");
                            $this->products->reduce_stock($product);
                        } catch (Exception $e) {
                            print_r("-> " . $e->getMessage() . "\n");
                        }
                        break;

                    case "SERVICE":
                        $this->products = new Products();
                        print_r("-> WATER, JUICE, SODA stock (separated by ,):\n");
                        $stockLine = readline("stock> ");
                        $stock = array_map('trim', explode(',', $stockLine));
                        foreach(array("WATER", "JUICE", "SODA") as $idx=>$product) {
                            $this->products->set($product, $stock[$idx]);
                        }
                        
                        $this->cash = new Cash();
                        print_r("-> 1, 0.25, 0.10, 0.05 coins (separated by ,):\n");
                        $coinsLine = readline("coins> ");
                        $coins = array_map('trim', explode(',', $coinsLine));
                        foreach(array(1, 0.25, 0.10, 0.05) as $idx=>$value) {
                            $n = (int)$coins[$idx];
                            if ($n > 0) {
                                foreach(range(0, $n-1) as $foo) {
                                    $this->cash->addCoin($value);
                                }
                            }
                        }
                        $this->cash->commit();
                        break;

                    default:
                        print_r("invalid command!\n");
                        $this->print_help();
                }
            }
        }
    }

    private function print_help(): void {
        print <<<END
        
        supported commands:
         0.05, 0.10, 0.25, 1 - insert money
         RETURN-COIN - returns all inserted money
         GET-WATER, GET-JUICE, GET-SODA - select item (Water = 0.65, Juice = 1.00, Soda = 1.50)
         SERVICE - a service person opens the machine and set the available change and how many items we have.
        
        END;
    }

    private function parse_commands(string $line): array {
        return array_map('trim', explode(',', $line));
    }
}


?>