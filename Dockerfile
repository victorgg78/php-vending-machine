ARG PHP_VERSION=7

# run tests in previous build step
FROM php:${PHP_VERSION}-cli-alpine 

WORKDIR /app
ADD ./app /app/

# install phpunit and run tests
RUN wget -O phpunit https://phar.phpunit.de/phpunit-9.phar \
&& chmod +x phpunit \
&& ./phpunit tests



# build app container
FROM php:${PHP_VERSION}-cli-alpine

WORKDIR /app
ADD ./app/src /app/

CMD [ "php", "./main.php" ]